#include <string.h>
#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <cassert>
#include <cstdarg>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <map>
#include <vector>
#include <sstream>
#include <iostream>
#include <sys/wait.h>
using std::cout; using std::cin; using std::endl; using std::cerr;
//#include <pwd.h>

/// Error/Exit Codes
const int SUCCESS = 0;
const int INPUT_TOO_LARGE = -1;
const int COMMAND_NOT_FOUND = -2;
const int NO_INPUT = -3;
const int INPUT_ERROR = -20;
const int POSIX_ERROR = -30;

const int COMMAND_FAILED = -40;
/// Error/Exit Codes

/// The continue flag value to continue the program
const int CONTINUE_FLAG = 0xfffffffd;

/// The length to be used for a buffer containing a pathname
const unsigned int PATH_SIZE = 256;

/// The maximum length of a command entered into the shell
const unsigned int COMMAND_INPUT_SIZE = 128;

/// The size of the buffer of the buffer writer.
const unsigned int BUFFER_WRITER_SIZE = 512;

/// The states that our SteveShell can be in.
enum ShellState{AwaitingCommand, ProcessingCommand,RunningCommand, Error, ExitingSuccess, ExitingFailure, StartingShell};


typedef std::vector<std::string> Args;


/*extern char ** SGetCommandArgv(SteveShell * );
extern int GetCommandArgc(SteveShell * );*/


/***************
 * Externs     *
 **************/
class SteveShell;
class CommandInstance;
class CommandPointer;
class CommandManager;

/***********
 * Classes *
 **********/


/**
 * Classes that can be used by the history tracker.
 * Must implement a ToString() function that allows the
 * item to be represented as a string.
 */
 class HistoryItem
 {
 public:
    /**
     * Get a string that represents the item.
     * @return A generated string that represents the history item.
     */
    virtual std::string ToString() = 0;

    virtual ~HistoryItem()
    {
    }

    friend std::ostream & operator<< (std::ostream & out, HistoryItem &item)
    {
        out<<item.ToString();
        return out;
    }

    friend std::ostream & operator<< (std::ostream & out, HistoryItem *item)
    {
        out<<item->ToString();
        return out;
    }
 };

/**
 * A stack-like, history tracking object that doesn't delete its elements.
 */
template <class T> class HistoryTracker
{
private:
    /// Container for the elements.
    std::vector<T*> mElements;

protected:
    /**
     * Get the container of the elements.
     * @returns The vector containing the history items.
     */
    std::vector<T*> & GetElements() { return mElements; }
public:
    /**
     * Default constructor.
     */
    HistoryTracker()
    {
       // If
       mElements = std::vector<T*>();
    }

    virtual ~HistoryTracker()
    {
        for  (unsigned int i = 0; i < mElements.size(); i++)
        {
            delete(mElements[i]);
        }
        Clear();
    }

    /**
     * Enter an item into the history.
     * @param element Item to put into the history
     */
    virtual void Push(T* element)
    {
        mElements.push_back(element);
    }

    T* Pop();
    /**
     * Get the ith most recent item in the history (the ith last
     * item to have been pushed into it).
     * @param i The order of the item to get.
     * @returns The object at the position, if it exists, or nullptr.
     */
    T* Get(unsigned int i = 1)
    {
        if (mElements.size() < i)
        {
            return nullptr;
        }
        return mElements.at(mElements.size() - i);
    }

    /**
     * Remove all items from the tracker.
     */
    virtual void Clear()
    {
        mElements.clear();
    }

    /**
     * Get an item from and index that starts at
     * the front of the container.
     * @param index The index of the item. index = 1
     * means that the first item in the container will
     * be found
     * @return The item if it exists, and nullptr if not.
     */
     T*  ReverseGet(unsigned int index = 1)
     {
         if (mElements.size() < index || index < 1)
         {
             return nullptr;
         }
         else
         {
             return mElements.at(index - 1);
         }
     }

    /**
     * Get the amount of objects in the history.
     * @returns Amount of objects in the history.
     */
    unsigned int GetHistorySize() { return mElements.size(); }


    /**
     * Get the n last items in the history in the following form:
     * "<index>: <item>\n"
     * @param n The amount of items to get.
     * @return A string in the format defined above.
     */
     std::string GetItemList(unsigned int n)
     {
         // If the amount of items in the history is less than n,
         // use the history size instead.
         n = (mElements.size() >= n) ? n : mElements.size();

         // Use a stringstream to capture each row.
        std::ostringstream oss;
        oss.str("");
        char * row = (char*) malloc(COMMAND_INPUT_SIZE * 2);
        for (unsigned int i = n; i > 0; i--)
        {
            oss << mElements.size() + 1 - i << ": " << Get(i) << endl;
        }
        free(row);
        return oss.str();
     }

};

class Directory : public HistoryItem
{
private:
    /// Absolute directory path
    std::string mPath;



public:
    Directory(std::string path)
    {
        mPath = path;

    }

    virtual ~Directory() 
    {

    }

    std::string ToString()
    {
        return mPath;
    }

    bool operator== (const Directory &other)
    {
        return (mPath.compare(other.mPath) == 0);
    }

};



class DirectoryManager : public HistoryTracker<Directory>
{
private:
    /// Directory we are currently in
    std::string mCurrentDirectory;

    /// Shell that owns this.
    SteveShell * mShell;

    /**
     * Set the current directory variable to the current
     * working directory.
     */
    void SetCurrentDirectory()
    {
        char * path = (char*) malloc(PATH_SIZE);
        getcwd(path, PATH_SIZE);
        mCurrentDirectory = std::string(path);
        free(path);
    }

    /**
     * Create a valid path from a token
     * @param token The token we are parsing.
     * @return A valid path
     */
    std::string GetPathFromToken(std::string token);

public:

    DirectoryManager(SteveShell * shell)
    {
        mShell = shell;
        SetCurrentDirectory();
    }

    virtual ~DirectoryManager()
    {
    }

    /**
     * Change the current directory.
     * @param token String that will be parsed for a path.
     * @return SUCCESS (0) if sucessful
     */
    int ChangeDirectory(std::string token)
    {
        // Parse a path from the token
        std::string path = GetPathFromToken(token);


        Directory * dir = new Directory(path);

        if (mCurrentDirectory.compare(dir->ToString()) == 0)
        {
            return SUCCESS;
            // Directories are the same, do nothing.
        }
        else
        {
            int e = chdir(dir->ToString().c_str());
            if (e == SUCCESS)
            {
                Push(new Directory(mCurrentDirectory));
                SetCurrentDirectory();
                return SUCCESS;
            }
            else
            {
                return POSIX_ERROR;
            }
        }
    }

    /**
     * Get the path of the current working directory.
     * @return The pathname as a string.
     */
    std::string GetCurrentPath()
    {
        return mCurrentDirectory;
    }



};







/**
 * Class that describes an error the shell encounters.
 */
class SteveError
{
private:

    /// The error code of this SteveError.
    int mErrorCode;



};

/**
 * An instance of a command that was entered by the user.
 *
 * Contains the argument tokens entered, and a pointer to the
 * command object that it will run using.
 */
class CommandInstance : public HistoryItem
{
    public:



        CommandInstance(CommandManager *manager, Args::iterator begin, Args::iterator end);
        virtual ~CommandInstance();
        CommandInstance(const CommandInstance& other);

        void ThrowError(int e);

        void ThrowError(int e, std::string mesg);

        SteveShell * GetShell();

        void SetManager(CommandManager *);

        void RunCommand();


        CommandManager * GetCommandManager();

        virtual std::string ToString();


    protected:

    private:
        /// The argument tokens of this command instance.
        Args mArgs;

        /// Pointer to the command manager that owns this.
        CommandManager * mCommandManager;

    public:
        const Args &GetArgs() {return mArgs; }
};

/**
 * Base class for all command pointer objects.
 * Command pointers are functors that take
 * a pointer to a CommandInstance object
 * when they run.
 */
class CommandPointer
{
    private:
        /// The maximum amount of args this command can take.
        int mMaxArgs; 
    public:
        CommandPointer() {}
        CommandPointer(int maxArgs): mMaxArgs(maxArgs) {}
        virtual ~CommandPointer();

        

        /**
         * Operator function
         * Function that is ran by the command instance
         * to run the command.
         * @param cInstance Pointer to CommandInstance running
         * the object.
         *
         * @returns SUCCESS (0) if successful, or some kind of error code.
         */
         virtual int operator() (CommandInstance * cInstance)  = 0;

         protected:
         bool TooManyArgs(int argCount)
         {
             if (argCount > mMaxArgs)
             {
                 cerr << "Too many arguments given! This command takes at most " << 
                    mMaxArgs << "." << endl; 

                return true;
             }
             return false;
         }



};

class CommandExternal : public CommandPointer
{
     private:

    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @return SUCCESS (0) if successful. Error code if not.
     */
   virtual  int operator() (CommandInstance * instance)
    {


        const Args argsVector = instance->GetArgs();

        // Check if the last argument is '&'
        std::string token = argsVector.back();
        bool background = (token == "&");
      


        char ** args = (char **) malloc(argsVector.size() + 1);

        for (unsigned int i = 0; i < argsVector.size(); i++)
        {
            args[i] = (char *) malloc(argsVector.at(i).length() + 1);
            strcpy(args[i], argsVector.at(i).c_str());
        }
        if (background)
       {     
           args[argsVector.size()-1] = NULL;
        }
        args[argsVector.size()] = NULL;

    
        
        int pid;
        int flag;

        pid = fork();

        if (pid < 0)
        {
            cerr << "Failed to run external command! Process could not be created!" << endl;
            return COMMAND_FAILED;
        }
        else if (pid > 0)
        {
            if (!background)
                waitpid(pid, NULL, 0);
            return SUCCESS;
        }
        else
        {
            flag = execvp(args[0], args);
            cerr << "Command " << args[0] << " was not found!" << endl;
            exit( flag );
        }    
        
        
        return SUCCESS;
    }
};




/**
 * Class that manages the listing and running of commands.
 */
class CommandManager : public HistoryTracker<CommandInstance>
{
    public:


        CommandManager(SteveShell *);
        virtual ~CommandManager();

        void AddCommandPointer(std::string, CommandPointer *);

        void EvaluateCommand(std::string);

        SteveShell * GetShell();

        unsigned int GetCommandNumber();

        CommandPointer * GetCommandFromCatalog(std::string);

        virtual void Push(CommandInstance * instance);

    protected:

    private:
        /// Map that contains all the CommandPointers that this manager can run
        std::map<std::string, CommandPointer *> mCommandCatalog;


        /// Pointer to the SteveShell that owns this.
        SteveShell * mShell;

        void CreateCommandInstance(Args::iterator, Args::iterator);

        void ExecuteCommand(CommandInstance *);

        /// Runs external commands
        CommandExternal * mExternalCommand;



};

/**
 * The shell itself.
 */
class SteveShell
{
    public:
        SteveShell();
        virtual ~SteveShell();

        std::string GetUserName() { return mUserName; }
        void SetmUserName(std::string val) { mUserName = val; }

        CommandManager * GetCommandManager();
        DirectoryManager * GetDirectoryManager();


        void Start();


        void ThrowError(int, std::string="");

        void ChangeShellState(ShellState);

        void ContinueShell();

    protected:

    private:
        /// Username of current user.
        std::string mUserName;

        /// Last line of input the user has entered.
        std::string mInputLine;

        /// Manages the parsing, execution, and logging of commands.
        CommandManager mCommandManager;

        /// Manages the parsing, changing, and logging of directories.
        DirectoryManager mDirectoryManager;

        /// Current state of the shell
        ShellState mState;

        void PrintPrompt();
        void SetUserName();
        void AwaitCommand();
        void DisplayError(const char * format, ...);
};


/**
 * Destructor
 */
CommandPointer::~CommandPointer()
{

    //dtor
}


/**
 * Constructor
 * @param manager The command manager that owns this.
 * @param begin Start of token vector.
 * @param end End of token vector.
 */
CommandInstance::CommandInstance(CommandManager *manager, Args::iterator begin, Args::iterator end)
{
    mCommandManager = manager;
    while (begin != end)
    {
        mArgs.push_back(*begin);
        ++begin;
    }
//    std::copy(begin, end, mArgs.begin());
}

/**
 * Copy constructor
 * @param other The instance we are copying.
 */
 CommandInstance::CommandInstance(const CommandInstance &other)
 {
     mCommandManager = other.mCommandManager;
     std::copy(other.mArgs.begin(), other.mArgs.end(), back_inserter(mArgs));
 }

/**
 * Destructor.
 */
CommandInstance::~CommandInstance()
{

}


/**
 * Throw an error to the shell from the command.
 * @param e Error code of the error.
 */
void CommandInstance::ThrowError(int e)
{
    GetShell()->ThrowError(e);
}

/**
 * Throw an error to the shell from the command.
 * @param e Error code of the error.
 * @param mesg Error message
 */
void CommandInstance::ThrowError(int e, std::string mesg)
{

}
/**
 * Set the command manager of this instance.
 * @param manager The manager that owns this.
 */
void CommandInstance::SetManager(CommandManager * manager)
{
    mCommandManager = manager;
}

/**
 * Get the shell.
 * @returns a pointer to the shell.
 */
SteveShell * CommandInstance::GetShell()
{
   return mCommandManager->GetShell();
}


/**
 * Run the command instance
 * If the command does not exist, do nothing.
 */
 void CommandInstance::RunCommand()
 {
     if (!mArgs.empty())
     {
         CommandPointer * cPtr = mCommandManager->GetCommandFromCatalog(mArgs[0]);
         if (cPtr != nullptr)
         {
             int e = (*cPtr)(this);
             if (e != SUCCESS)
             {
                 ThrowError(e);
             }
         }
         else
         {
             std::cerr << "Invalid command: "  << mArgs[0] << endl;
             ThrowError(COMMAND_NOT_FOUND);
         }
     }
     mCommandManager->Push(this);
 }


/**
 * Get the command manager that owns this instance.
 * @returns Pointer to the parent command manager.
 */

 CommandManager * CommandInstance::GetCommandManager()
 {
     return mCommandManager;
 }


/**
 * Convert this command instance into a string.
 * @return A string containing the tokens of the command.
 */
 std::string CommandInstance::ToString()
 {
     // Use a string stream to input each argument into the string.
     std::ostringstream oss;
     oss.str("");
     Args::iterator iter = mArgs.begin();
     for ( ; iter != mArgs.end() - 1; ++iter)
     {
         oss << *iter << ' ';
     }
     oss << *iter;

     return oss.str();
 }

 /**
 * Constructor.
 * @param stShell The shell that owns this.
 */
CommandManager::CommandManager(SteveShell * stShell) : HistoryTracker()
{
    mShell = stShell;
    mExternalCommand = new CommandExternal();

}

CommandManager::~CommandManager()
{

    for (std::map<std::string, CommandPointer*>::iterator iter = mCommandCatalog.begin();
    iter != mCommandCatalog.end();
    ++iter)
    {
        delete iter->second;
    }
    delete mExternalCommand;
}


/**
 * Get the command number of the next command to be entered.
 * Used for printing the prompt.
 * @returns The command number of the next command to be entered.
 */
 unsigned int CommandManager::GetCommandNumber()
 {
     return GetHistorySize() + 1;

 }

/**
 * Evaluate a line of user input, and try to create
 * an instance of a command from it.
 *
 * @param input The line of user input.
 */
void CommandManager::EvaluateCommand(std::string input)
{

    std::vector<std::string> args = std::vector<std::string>();


    std::string token;

    std::ostringstream tokenParser;
    tokenParser.str("");

    for (unsigned int i = 0; i < input.length(); i++)
    {
        char c = input.at(i);

        if (c == '\t' || c == ' ' || c == '\r')
        {
            token = tokenParser.str();
            if (token.compare("") != 0)
            {
                args.push_back(token);
                tokenParser.str("");
            }
        }
        else
        {
            tokenParser << c;
        }
    }
    token = tokenParser.str();
    if (token.compare("") != 0)
    {
        args.push_back(token);
    }

    if (args.size() > 0)
    {
        if (args.size() == 1)
        {
            if (args.at(0)[0] == '!')
            {
                std::string commandNumString = args.at(0).substr(1);
                int commandNum = atoi(commandNumString.c_str());

                CommandInstance * instance =  ReverseGet(commandNum);
                if (instance != nullptr)
                {
                    CommandInstance * i = new CommandInstance(*instance);
                    i->RunCommand();
                }
                else
                {
                    std::cerr << "Command #" << commandNum << " does not exist!" << std::endl;
                }
            }
            else
            {
                CreateCommandInstance(args.begin(), args.end());
            }

        }
        else
        {

            CreateCommandInstance(args.begin(), args.end());
        }
    }

}


/**
 * Create an instance of a command, then run it if the command exists.
 * @param begin Start of the vector of tokens for the commmand.
 * @param end End of the vector of tokens.
 */
 void CommandManager::CreateCommandInstance(Args::iterator begin, Args::iterator end)
 {
     CommandInstance * instance = new CommandInstance(this, begin, end);
     instance->RunCommand();

 }


 /**
 * Get the shell.
 * @returns a pointer to the shell.
 */
SteveShell * CommandManager::GetShell()
{
   return mShell;
}

/**
 * Gets a command from the catalog.
 * @param name Name of the command
 * @returns Pointer to the command or nullptr if it does not exist.
 */
 CommandPointer * CommandManager::GetCommandFromCatalog(std::string name)
 {
     std::map<std::string, CommandPointer *>::iterator iter = mCommandCatalog.find(name);
     if (iter == mCommandCatalog.end())
     {        
         return mExternalCommand;
     }
     return iter->second;
 }


 /**
  * Add a command pointer to the command catalog. This will throw
  * an Assert error if a command has already been defined with that name.
  * @param name Name of the command
  * @param cPtr Pointer to the command pointer object.
  */
void CommandManager::AddCommandPointer(std::string name, CommandPointer * cPtr)
{
    // Command names must be unique!
    assert(mCommandCatalog.find(name) == mCommandCatalog.end());
    mCommandCatalog.insert(std::pair<std::string, CommandPointer *>(name, cPtr));
}

/**
 * Add a command instance to the history
 * @param instance CommandInstance to add.
 */
 void CommandManager::Push(CommandInstance * instance)
 {
     HistoryTracker::Push(instance);
 }






/**
 * Constructor.
 */
SteveShell::SteveShell() : mCommandManager(this), mDirectoryManager(this)
{

    mState = StartingShell;
    SetUserName();
}

/**
 * Destructor.
 */
SteveShell::~SteveShell()
{

}


/**
 * Set the username value of the shell to the current user
 */
void SteveShell::SetUserName()
{

    char * uName;
    #ifdef __linux__
        uName = cuserid(NULL);
    #else
        uName = (char *) malloc(5);
        strcpy(uName, "test");
        #endif

    mUserName = std::string(uName);
}


/**
 * Have the SteveShell print out a prompt in the following format:
 * "<{command number} {username}> "
 */
void SteveShell::PrintPrompt()
{
    // Get command number
    unsigned int cNumber = mCommandManager.GetCommandNumber();

    cout << endl << "<" << cNumber << " " << mUserName << "> ";



}

/**
 * Start the shell and keep it running until
 * an exit state is reached.
 */
void SteveShell::Start()
{
    while (mState != ExitingFailure && mState != ExitingSuccess)
    {
        ContinueShell();
    }
}


/**
 * Continue the shell for one command cycle.
 */
 void SteveShell::ContinueShell()
 {
     PrintPrompt();
     ChangeShellState(AwaitingCommand);
 }


 /**
  * Change the state of the shell.
  * @param state New state of our SteveShell
  */
void SteveShell::ChangeShellState(ShellState state)
{
    // Determine what to do before exiting current state.
    switch (mState)
    {
        case RunningCommand:

            break;
        case Error:
            break;
        default:
            break;
    }

    // Swith the state to the new state.
    mState = state;





    // Determine what to do upon entering new state.
    switch (mState)
    {
        case RunningCommand:
            //RunCommand();
            break;
        case AwaitingCommand:

            AwaitCommand();
            break;
        case ProcessingCommand:
            mCommandManager.EvaluateCommand(mInputLine);
            break;
        case Error:

            //GenerateError();
            break;

        default:
            break;
    }
}

/**
 * Await input from user
 */
 void SteveShell::AwaitCommand()
 {



     std::getline(std::cin, mInputLine);

     ChangeShellState(ProcessingCommand);
 }


 /**
    * Display an error the shell has encountered to the user, then print it.
    * @param format Format of the error message.
    * @param ... Arguments for format.
    */
void SteveShell::DisplayError(const char * format, ...)
{

    write(2, "Error: ", 7);

    va_list args;

    va_start (args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    putchar('\n');
}

 /**
  * Throw a shell error and switch to outputting error.
  * @param e Error code
  * @param mesg Error message
  */
  void SteveShell::ThrowError(int e, std::string mesg)
  {
      // Buffer for error message. Each case will decide what
    // size this buffer is.

    switch (e)
    {
        case INPUT_TOO_LARGE:
            DisplayError("User input too large! Must be less than %d characters", COMMAND_INPUT_SIZE);
            break;

        case COMMAND_NOT_FOUND:
            DisplayError("Command was not found.");
            break;

        case POSIX_ERROR:
            DisplayError("POSIX error occured, %s", strerror(errno));
    }

  }


CommandManager * SteveShell::GetCommandManager() { return &mCommandManager; }
DirectoryManager * SteveShell::GetDirectoryManager(){ return &mDirectoryManager; }


/**
 * Create a valid path from a token
 * @param token The token we are parsing.
 * @return A valid path 
 */
std::string DirectoryManager::GetPathFromToken(std::string token)
{
    std::string path = "";
    if (token.empty())
        return path;

    char tag = token[0];

    if (tag == '~')
    {

        if(token.length() > 1)
        {

            // Check for '~/'
            unsigned int offset = 1;
            if (token.at(1) == '/')
            {
                offset++;
            }


            #ifdef __linux__
            path = "/user/" + token.substr(offset);
            #else
            path = "C:/Users/" + token.substr(offset);
            #endif
            return path;
        }
        else
        {
            #ifdef __linux__
            path = "/user/" + mShell->GetUserName();
            #else
            path = "C:/Users/Samantha";
            #endif
            return path;
        }
    }
    if (tag == '#')
    {
        std::string indexString = token.substr(1);
        unsigned int index = atoi(indexString.c_str());

        std::vector<Directory *> elements = GetElements();
        if (elements.size() < index || index < 1)
        {
            std::cerr << "There is no directory at index " << index << "!" << std::endl;
        }
        else
        {
            HistoryItem * dir = elements.at(index-1);
            path = dir->ToString();
        }
        return path;
    }

    return token;
}


/**
 * Change Directories
 */
 class CommandCd : public CommandPointer
 {
 public:
    CommandCd() : CommandPointer(2) {}

 private:
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @returns SUCCESS (0) if successful. Error code if not.
     */
    virtual int operator() (CommandInstance * instance)
    {
        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }
        std::string token = "~";

        Args tokens = instance->GetArgs();

        if (tokens.size() > 1)
        {
            token = tokens[1];
        }

        int e = instance->GetShell()->GetDirectoryManager()->ChangeDirectory(token);

        return e;
    }
 };





 /**
 * Exit the shell
 */
 class CommandQuit : public CommandPointer
 {
 public:
     CommandQuit(): CommandPointer(1) {}

 private:
 
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @returns SUCCESS (0) if successful. Error code if not.
     */
    virtual int operator() (CommandInstance * instance)
    {
        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }
        instance->GetShell()->ChangeShellState(ExitingSuccess);

        return SUCCESS;
    }
 };



 /**
 * List previous directories
 */
 class CommandDlist : public CommandPointer
 {
 public:
     CommandDlist(): CommandPointer(1) {}

 private:
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @returns SUCCESS (0) if successful. Error code if not.
     */
    virtual int operator() (CommandInstance * instance)
    {
        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }

        std::string dirList = instance->GetShell()->GetDirectoryManager()->GetItemList(10);
        cout << dirList;
        return SUCCESS;
    }
 };

 /**
 * List current directory
 */
 class CommandCurr : public CommandPointer
 {
 public:
     CommandCurr(): CommandPointer(1) {}

 private:
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @returns SUCCESS (0) if successful. Error code if not.
     */
   virtual  int operator() (CommandInstance * instance)
    {

        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }

        std::string curr = instance->GetShell()->GetDirectoryManager()->GetCurrentPath();
        cout << curr;
        return SUCCESS;
    }
 };


 /**
 * List/manipulate environment variables
 */
 class CommandEnv : public CommandPointer
 {
 public:
     CommandEnv(): CommandPointer(3) {}

 private:
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @returns SUCCESS (0) if successful. Error code if not.
     */
    virtual int operator() (CommandInstance * instance)
    {

        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }

         const Args &tokens = instance->GetArgs();
         unsigned int token_count = tokens.size();

         if (token_count == 1)
         {
            if (environ[0] == NULL)
            {
                cout << endl;
            }
            else
            {
                std::cout << environ[0] << endl;
                int i = 1;
                while (environ[i] != NULL)
                {

                    cout << endl  << environ[i];
                    i++;

                }

            }


         }
         else if (token_count == 2)
         {
             const char * envVar = getenv(tokens[1].c_str());
             if (envVar != NULL)
             {
                 cout << envVar;

             }
             else
             {

                 cerr << "Environment variable \'" <<  tokens[1] << "\'is undefined." << endl;
             }
         }

         else if (token_count == 3)
         {
            #ifdef __linux__
            int e = setenv(tokens[1].c_str(), tokens[2].c_str(), 1);
             if (e != SUCCESS)
             {
                 return POSIX_ERROR;
             }
             #else
              cout << "Setting " << tokens[1] << " to " << tokens[2] << "." << endl;
            #endif

         }

        return SUCCESS;
    }
 };


 /**
 * Display date and time
 */
 class CommandDate : public CommandPointer
 {
 public:
     CommandDate(): CommandPointer(1) {}

 private:
    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @return SUCCESS (0) if successful. Error code if not.
     */
    virtual int operator() (CommandInstance * instance)
    {

        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }

         time_t t = time(NULL);
        char * readableDate = ctime(&t);

        cout << readableDate;

        return SUCCESS;
    }
 };


 /**
 * Display last 10 commands.
 */
 class CommandClist : public CommandPointer
 {
 public:
     CommandClist(): CommandPointer(1) {}
 private:

    /**
     * Run the command.
     *
     *
     * @param instance CommandInstance running this command.
     * @return SUCCESS (0) if successful. Error code if not.
     */
   virtual  int operator() (CommandInstance * instance)
    {

        if (TooManyArgs(instance->GetArgs().size()))
        {
            return COMMAND_FAILED;
        }

        CommandManager * manager = instance->GetCommandManager();
        std::string commandList = manager->GetItemList(10);
        cout << commandList;


        return SUCCESS;
    }
 };





int main()
{
    SteveShell stShell = SteveShell();
    stShell.GetCommandManager()->AddCommandPointer("date", new CommandDate());
    stShell.GetCommandManager()->AddCommandPointer("clist", new CommandClist());
    stShell.GetCommandManager()->AddCommandPointer("env", new CommandEnv());
    stShell.GetCommandManager()->AddCommandPointer("quit", new CommandQuit());
    stShell.GetCommandManager()->AddCommandPointer("curr", new CommandCurr());
    stShell.GetCommandManager()->AddCommandPointer("dlist", new CommandDlist());
    stShell.GetCommandManager()->AddCommandPointer("cd", new CommandCd());
    stShell.Start();

    return 0;
}

